#include <cstdlib>
#include <vector>
// #include <set>
#include <list>
#include <array>
#include <map>
// #include <typeinfo> //for printout type of variable
#include <cmath>
#include <cassert>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "ublasMatrix.hpp"

#define maxDim 6
#define eps 5.0e-07

typedef uint64_t ty;
typedef std::array<float,3> Point3f;

namespace ublas = boost::numeric::ublas;

inline Point3f diff(const Point3f& bgnn, const Point3f& ennd)
{
    return std::move(Point3f{{ennd[0]-bgnn[0],ennd[1]-bgnn[1],ennd[2]-bgnn[2]}});
}

inline float ABS(const Point3f& u)
{
    return sqrt( u[0]*u[0]+u[1]*u[1]+u[2]*u[2] );
}

inline float quadrat(const Point3f& u)
{
    return  u[0]*u[0]+u[1]*u[1]+u[2]*u[2] ;
}

inline void printPoint(const Point3f& p)
{
    using namespace std;
    cout<< p[0] << ", "<< p[1]<< ", "<<  p[2] << endl;  
}


typedef struct cell_t
{   
    // bool complete=0;
    ty ind=0;
    Point3f centroid;
    std::vector<ty> adjCells;
    std::vector<ty> adjBCFaces;
    short geomType;
    // std::vector<float> GTD;
    ublas::matrix<float> GTD;
    short fromCells;
    short fromFaces;
public:
    explicit cell_t():ind(0),geomType(0),fromCells(0),fromFaces(0)
    {   
        // this->ind=0;
        // this->geomType=0;
        this->adjCells.reserve(maxDim);
        this->adjBCFaces.reserve(maxDim);
        // this->GTD.reserve(3*maxDim);
        this->GTD=ublas::matrix<float>(3,maxDim);
        // this->GTD.swap(ublas::matrix<float>(3,maxDim));
    }
    cell_t(const cell_t & )=delete;
    cell_t & operator=(const cell_t&) = delete;
    cell_t(cell_t && )=default;
    cell_t & operator=(cell_t&& )=default;
    // ~cell_t(){} //destructor prevents implicit move assignment
}cell_t;



struct meshVctr
{
    // bool complete=0;

    ty Ncells=0;


    std::multimap<ty,ty> cells_VS_Adj_BC_Faces; //cell_ind vs face_ind

    std::map<ty, int> bcFacesTypes; // < ty face_index, int bcType>
    
    std::map<int, const std::string> bcTypes; // 

    std::map<ty, Point3f> all_BC_Face_Centroids; //bc_face_ind vs its.centroid

    std::vector<cell_t> Mesh;

    // std::vector<upCell_t> Mesh;
    // std::vector
public:
    void calculate_G_Td()
    {
        for(auto& ct :this->Mesh)
        {
            assert(ct.geomType>0);
            ublas::matrix<float>D(ct.geomType,3);
            Point3f d;
            short k=0;
            for ( auto it : ct.adjCells)
            {
                d= diff(ct.centroid,this->Mesh[it].centroid);
                D(k,0)=d[0];
                D(k,1)=d[1];
                D(k,2)=d[2];
                ct.fromCells++;
                ++k;
            }
            // std::cout<< "++++++faces: +++++++++\n";
            for(auto it:ct.adjBCFaces)
            {
                auto search=this->all_BC_Face_Centroids.find(it);
                if(search!=this->all_BC_Face_Centroids.end())
                {
                    d=diff(ct.centroid, search->second);    
                    D(k,0)=d[0];
                    D(k,1)=d[1];
                    D(k,2)=d[2];
                    ct.fromFaces++;
                    ++k;
                }
            }
            if (k<ct.geomType)
            {
                std::cout<< "RESIZE_BACK!\n";//...if u sic fak
                D.resize(k,3);
            }
            // auto nr_D=D.size1();
            ublas::matrix<float> Dtr=trans(D);
            ublas::matrix<float> G=prod(Dtr,D);
            ublas::matrix<float> invG(3,3);
            InvertMatrix(G,invG);
            ublas::matrix<float> GTD= prod(invG,Dtr);

            ct.GTD.swap(GTD);
        }
    }
    void Info(int verbocity=1)
    {   
        using namespace std;
        cout<<"Ncells  = "<< this->Ncells <<endl;
        
        cout<<"Mesh.size()==Ncells ? "<< (this->Mesh.size() == this->Ncells ? "TRUE" : "FALSE") <<endl; 
        
        cout<< "cells_VS_Adj_BC_Faces.size() = " << this-> cells_VS_Adj_BC_Faces.size()<<endl;
        cout<< "all_BC_Face_Centroids.size() = " << this-> all_BC_Face_Centroids.size()<<endl;

        cout<< "bcFacesTypes.size() = " << this-> bcFacesTypes.size()<<endl;

        cout<< "Patches's BC_types:\n"; 
        for(const auto& kt: this->bcTypes)
        {
            cout<< "Patch " <<  kt.first << "  :" <<  kt.second << endl;
        }
        cout<< "\n\n--------------------\n";
        
        if(verbocity > 0)
        {
            cout<<"\nCells:\n";
            
            for (const auto& jt : this->Mesh)
            {
                cout <<"cell # "<< jt.ind << endl;
                cout <<"  geomType = "<<jt.geomType<<endl;
                cout<< "  centroid: "<< "( "<< jt.centroid[0]<<"  "<< jt.centroid[1]<< "  "<<  jt.centroid[2] << " )"<< endl;
                cout<< "     adjCells :{ ";
                for (const auto& ii : jt.adjCells)
                {
                    cout<<ii <<" ";
                }

                cout<<" }"<<endl<<endl;
                cout<< "     adj_BC_Faces :{ ";
                for (const auto& tt : jt.adjBCFaces)
                {
                    cout<<tt <<" ";
                }
                cout<<" }"<<endl<<endl;

                if (verbocity >1)
                {
                    printMatrix(jt.GTD,"GTD:\n");            
                }
            }
        }
    }
};


typedef struct cellData_t
{
    ty ind;
    float T;        //scalarField
    Point3f gT;     //vectorField
    Point3f gT_th;  //vectorField   
    float R;        //scalarField

}cellData_t;


struct dataVector
{
    // bool complete=0;
    std::vector<cellData_t> cells;
public:
    explicit dataVector(const meshVctr& MV )
    {
        // this->cells.reserve(MV.Ncells);
        this->cells=std::vector<cellData_t>(MV.Ncells);
        // this->cells.reserve(MV.Ncells);
        // std::cout<<"this->cells.size() = "<<  this->cells.size()<<std::endl;

        auto it = cells.begin();
        for(const auto& jt: MV.Mesh)
        {       
            it->ind=jt.ind;
            
            it->T=quadrat(jt.centroid);//T==x**2+y**2+z**2
            
            it->gT_th[0]=jt.centroid[0]*2; // gT_th=grad(T)=={2*x, 2*y, 2*z} 
            it->gT_th[1]=jt.centroid[1]*2;
            it->gT_th[2]=jt.centroid[2]*2;
            ++it;
        }

        ///davaj po-novoj zapryagat':
        it= cells.begin();
        for(const auto& jt: MV.Mesh)
        {       
            ublas::vector<float>vT(jt.geomType);
            // vT*=float(0);
            ty pos=0;
            for(auto k: jt.adjCells)
            {
                vT(pos++)= this->cells[k].T - it->T;
            }
            auto Gr=prod(jt.GTD,vT);

            it->gT[0]=Gr(0);
            it->gT[1]=Gr(1);
            it->gT[2]=Gr(2);

            it->R=ABS(it->gT)-ABS(it->gT_th); //R == |gT| - |gT_th|

            ++it;
        }
    }
};

