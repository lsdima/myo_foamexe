#ifndef __UBLASMATRIX_HPP_INCLUDED__
#define __UBLASMATRIX_HPP_INCLUDED__

#include <iostream>
#include <iomanip> //setw
////////////////
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
// using namespace boost::numeric;

namespace ublas = boost::numeric::ublas;
// using namespace boost::numeric::ublas;

typedef std::size_t ty;



 /* Matrix inversion routine.
 Uses lu_factorize and lu_substitute in uBLAS to invert a matrix */
template<class T>
bool InvertMatrix(const ublas::matrix<T>& input, ublas::matrix<T>& inverse)
{
    // using namespace boost::numeric::ublas;
    // using namespace std;

    typedef ublas::permutation_matrix<std::size_t> pmatrix;

    // create a working copy of the input
    ublas::matrix<T> A(input);

    // create a permutation matrix for the LU-factorization
    pmatrix pm(A.size1());

    // perform LU-factorization
    int res = ublas::lu_factorize(A, pm);
    if (res != 0)
        return false;

    // create identity matrix of "inverse"
    inverse.assign(ublas::identity_matrix<T> (A.size1()));

    // backsubstitute to get the inverse
    ublas::lu_substitute(A, pm, inverse);

    return true;
}


// template<class T>
// void vec2Row(const std::vector<T> & v,ublas::matrix<T>& m,ty k) 
// {
//    if(k>m.size1()-1){return;}
//    typename std::vector<T>::const_iterator it=v.begin();
//    ty j=0;
//    while(j<m.size2() && it!=v.end())
//    {
//         m(k,j)=*it;
//         ++it;
//         ++j;
//    }
// }

// template<typename T,typename F=ublas::row_major>
// ublas::matrix<T, F> makeMatrix(std::size_t nr, std::size_t nc, const std::vector<T>& v)
// {
//     if(nr*nc!=v.size())
//     {
//         std::vector<T> u(v);
//         u.resize(nr*nc);
//         return makeMatrix<T,nr,nc>(u);
//     } // Handle this case
//     ublas::unbounded_array<T> storage(nr*nc);
//     // ublas::bounded_array<T,nr*nc> storage(nr*nc); //??????
//     std::copy(v.begin(), v.end(), storage.begin());
//     return std::move(ublas::matrix<T>(nr, nc, storage));
// }

// template <typename T , template <typename...> typename C, typename F=ublas::row_major>
// ublas::matrix<T,F,C<T> > makeMatrix(std::size_t nr, const C<T>& v)
// {
//     size_t nc = v.size()/nr;
//     ublas::matrix< T, F, C<T> > M(nr,nc);
//     M.data() = v;

//     return std::move(M);
// }



// template <typename T>
// void matrix2Vector(const ublas::matrix<T>& M , std::vector<T>& vec)
// {
//     // std::size_t len=(M.size1())*(M.size2());
//     // std::vector<T>result;
//     // result.reserve(len);
//     for(std::size_t i=0;i<M.size1();++i)
//     {
//         for(std::size_t j=0;j<M.size2();++j)
//         {
//             vec.emplace_back(M(i,j));
//         }
//     }
//     // return std::move(result);
// }


// template<typename T> 
// void printVec(const std::vector<T>& v)
// {
//     using namespace std;
//     cout<< "vector sizeof "<< v.size() << " contain:" <<endl; 
//     for (size_t i=0; i<v.size();++i )
//     {
//         auto e=v.size()-1;
//         cout << v[i];
//         cout<< (i<e ? ", " : " ;\n\n") ;
//     }
// }


template<typename T, template <typename ...> typename C > 
void printVec(const C<T>& v)
{
    using namespace std;
    cout<< "vector sizeof "<< v.size() << " contain:" <<endl; 
    for (size_t i=0; i<v.size();++i )
    {
        auto e=v.size()-1;
        cout << v[i];
        cout<< (i<e ? ", " : " ;\n\n") ;
    }
}


template <typename T, template <typename...> typename C,typename F=ublas::row_major>
void printMatrix(const ublas::matrix<T,F,C<T>> &m,const std::string&str="")
{
    std::cout<<"\n";
    std::cout<<"\t"<<str<<"("<<m.size1()<<","<<m.size2()<<")"<<"\n\n";
    for(ty i=0;i<m.size1();++i)
    {
        std::cout <<i<<": ";
        for (ty j=0;j<m.size2();++j)
        {
            std::cout<<std::setw(5)<<m(i,j)<<"  ";
        }
        std::cout<<"\n";
    }
    std::cout<<"\n\n";
}





#endif
//////////////////////////////////////////////////////////////////////////////

/*
/////////////////////////////////////////////////////////////////////////////
int main()
{
	// Начальная инициализация матрицы a
	ublas::matrix<double> A(2,2);
    A(0,0)=1.;
    A(0,1)=2.;
    A(1,0)=3.;
    A(1,1)=4.;
    printMatrix(A,"A:");

    // матрица b - единичная
    ublas::matrix<double> B(2,2);
    B(0,0)=1.;
    B(0,1)=0.;
    B(1,0)=0.;
    B(1,1)=1.;
    printMatrix(B,"B:");

    // перемножение двух матриц
    ublas::matrix<double> C = prod(B, A);
    printMatrix(C,"c=A*B:");

    // результат обращения матрицы a записан в матрицу b
    InvertMatrix(A, B);
    printMatrix(B,"B=invert A:");

    std::cout<<"*****************************************"<<std::endl;    
    ublas::matrix<float> Q(3,3);
    Q(0,0)=1.;
    Q(0,1)=2.;
    Q(0,2)=3.;
    Q(1,0)=4.;
    Q(1,1)=5.;
    Q(1,2)=6.;
    Q(2,0)=1;
    Q(2,1)=-1;
    Q(2,2)=7.;
    
    printMatrix(Q,"Q =");
    ublas::matrix<float> invQ(3,3);


    InvertMatrix(Q,invQ);
    printMatrix(invQ,"invQ =\n");
    ublas::matrix<float> QQ_=prod(Q,invQ);
    printMatrix(QQ_,"QQ_ =\n");

    std::cout<<"*****************************************"<<std::endl;    
    ublas::matrix<double> P(3,3);
    P(0,0)=1.;
    P(0,1)=2.;
    P(0,2)=3.;
    P(1,0)=4.;
    P(1,1)=5.;
    P(1,2)=6.;
    P(2,0)=1;
    P(2,1)=-1;
    P(2,2)=7.;
    
    printMatrix(P,"P =");
    ublas::matrix<double> invP(3,3);

    InvertMatrix(P,invP);
    printMatrix(invP,"invP =\n");
    ublas::matrix<double> PP_=prod(P,invP);
    printMatrix(PP_,"PP_ =\n");
    return 0;
}

*/

    /*// перемножение матрицы a и обратной ей матрицы = единичная матрица
    C = prod(B, A);
    printMatrix(C,"C=B*A:");

    // произведение матриц и векторов
    ublas::vector<double> v(3);
    v(0)=-4;
    v(1)=-35;
    v(2)=-101;
    std::cout<<"v:\n";

    std::cout<<v<<endl;

    ublas::vector<double> w;
    w = prod(A, v); 
    std::cout<<"w:\n";

    std::cout<<w<<endl;


    std::vector<double> v;
    ty j=21;
    for (int i = 0; i < 10; ++i)
    {
        v.push_back(double(j));
        ++j;
    }
*/
    // for(std::vector<double>::iterator it=v.begin();it!=v.end();++it)
    // {
    //     std::cout<<"*it="<<*it<<"\n";
    // }

    // ublas::matrix<double> m;
    // m.resize(3,12);
    // m.clear();
    // vec2Row(v,m,0);
    // printMatrix(m);
    // vec2Row(v,m,1);
    // printMatrix(m);





/////////////////////////////////////////////////////////////////////////////
/*
// Примеры использования матричных операций

    // создание матрицы (пустой конструктор)
    matrix<double> A;
    // изменение размеров матрицы (значение существующих элементов сохраняются)
    A.resize(3,3);
    // создание матрицы с заданными размерами 
    matrix<double> B(3,3), C(3,3);
    // кол-во строк матрицы
    int i=A.size1(); 
    // кол-во столбцов матрицы
    int j=A.size2();

    

    // сложение и вычитание матриц (сложение и вычитание векторов – аналогично)
    C = A + B; 
    C = A - B; 
    C = -A;
    C += A; 
    C -= A; 
    
    // умножение/деление матрицы на число ( умножение/деление вектора на число – аналогично)
    double x(5);
    C = x * A; 
    C = A * x; 
    C = A / x;
    C *= x; 
    C /= x; 
    
    // произведение матриц и векторов
    vector<double> v(3), w;
    w = prod(A, v); 
    w = prod(v, A); 
    C = prod(A, B); 
    
    // транспонирование матрицы 
    C = trans(A); 
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////    


// 1:
// (compiler flags added):
// -DNDEBUG -DBOOST_UBLAS_NDEBUG 
// For a quick benchmark we again limit mm to 500500 with -DM_MAX=500. This time we also enable the optimized micro kernel for AVX with -DHAVE_AVX:
// $shell> g++ -Ofast -mavx -Wall -std=c++11 -DNDEBUG -DHAVE_AVX -DM_MAX=500 -I ../boost_1_60_0 matprod.cc


// 2:
// You could help the library a little, declaring
// matrix<int, column_major> B;

// 3:
// P.S. Having read uBLAS documentation to the end ;), you should have found out that there's actually a dedicated function
//  to multiply whole matrices at once. 2 functions - axpy_prod and opb_prod.
//   So:
//    opb_prod(A, B, C, true);
// even on unoptimized row_major B matrix executes in 8.091 sec and is on par with your vector algorithm

// P.P.S. There's even more optimizations:

// C = block_prod<matrix<int>, 1024>(A, B);
// executes in 4.4s, no matter whether B is column_ or row_ major.
// Consider the description: "The function block_prod is designed for large dense matrices." Choose specific tools for specific tasks!
// ******************************************************************************


    // boost::numeric::ublas::vector<int> v(10);
    // boost::numeric::ublas::matrix<int> m(10,10);  //using v.size() also works
    // std::copy(v.begin(), v.end(), m.begin1());

// #include <algorithm>
// #include <vector>
// #include <boost/numeric/ublas/storage.hpp>
// #include <boost/numeric/ublas/matrix.hpp>
// #include <boost/numeric/ublas/io.hpp>

// namespace ublas = boost::numeric::ublas;

// template <typename T, typename F=ublas::row_major>
// ublas::matrix<T, F> makeMatrix(std::size_t m, std::size_t n, const std::vector<T> & v)
// {
//     if(m*n!=v.size()) {
//         ; // Handle this case
//     }
//     ublas::unbounded_array<T> storage(m*n);
//     std::copy(v.begin(), v.end(), storage.begin());
//     return ublas::matrix<T>(m, n, storage);
// }

