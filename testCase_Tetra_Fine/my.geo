lc = 0.2;
L=10.0;
B=10.0;
H=20.0;
Point(1) = { L/2,  L/2, -H/2, lc};
Point(2) = { L/2, -L/2, -H/2, lc};
Point(3) = {-L/2, -L/2, -H/2, lc};
Point(4) = {-L/2,  L/2, -H/2, lc};
//
// Point(5) = { L/2,  L/2,  H/2, lc};
// Point(6) = { L/2, -L/2,  H/2, lc};
// Point(7) = {-L/2, -L/2,  H/2, lc};
// Point(8) = {-L/2,  L/2,  H/2, lc};
// 
// Curve Loop (1)
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

//LINE_LOOPs and PLANE_SURFACEs:

Curve Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};
//
// ///TRANSFINITE:
// r=1;
// Transfinite Line {1,2} = r  Using Bump 10;
// Transfinite Line {3,4} = r  Using Bump 10;

// Transfinite Surface {1};
// // Recombine Surface {100:108};



e() = Extrude {0, 0, H}{ Surface{6}; };


// Whole domain
// Four "terminals" of the model
Physical Surface("wall_1") = {6};

// Physical Surface(7) = {e(2)};
// Physical Surface(8) = {e(3)};
// Physical Surface(9) = {e(4)};
// Physical Surface(10) = {e(5)};
// Physical Surface(11) = {e(1)};


Physical Surface("wall_2") = {28};
Physical Surface("wall_3") = {15};
Physical Surface("wall_4") = {19};
Physical Surface("wall_5") = {23};
Physical Surface("wall_6") = {27};

// Physical Volume("fluid") = {e(1)};
Physical Volume("fluid") = {e(1)};


// Physical Surface(75) = {e(7)};
// Physical Volume("internal") = {1:9};
// Physical Surface("front") = {130,12,218,240,262,284,306,196,174};
// Physical Surface("back") =  {100:108};
// Physical Surface("inlet") = {161,187,305};
// Physical Surface("outlet") = {301,279,257,235,213,191};
// Physical Surface("WING") = {121};//+
meshSizeMax=3.0;
meshSizeMin=1.0;

Field[1] = Box;
//+
// Field[2] = Attractor;
//+
Field[1].XMax = H;
//+
Field[1].XMin = -H;
//+
Field[1].YMax = H;
//+
Field[1].YMin = -H;
//+
Field[1].ZMax = H;
//+
Field[1].ZMin = -H;
//+
Field[1].Thickness = meshSizeMax;
//+
Field[1].VIn = meshSizeMin;
//+
Field[1].VOut = meshSizeMax;
//+
Background Field = meshSizeMax;
//+
// Show "*";

//
Mesh.MshFileVersion=2.2;
//
// Mesh 3;

//todo then:
// gmsh my.geo -3 -o tet.msh
// gmshToFoam tet.msh -case case