if {X,Y,Z } -the coordinates of each cell's centroid, 

scalar T =X^2+Y^2+Z^3

vector gT_th ={2x,2y,2z}  - definite value for  grad(T)

vector gT - numerically calculated grad(T)

scalar R == |gT| - |gT_th|  (по условию!)


картинки и testCase_Tetra_Fine_1.vtk были получены на сетке в 825530 ячеек(tetra) 

Для gT и gT_th - значения в граничных cells определены некорректно. т.к.  изначально для них были заданы BC некорректно (no_sLip_Wall);
но это не суть...


# TODO:

./Allwmake
# then try to run one of test cases:
cd ./testCase_Tetra_Fine/
./Allclean
./Allrun

# run paraViwew:

paraFoam

# or just run:

foamToVTK

# enjoy! 


//////////////////////////////


for boost/ublas/numeric to run well, use comile flags:  -DNDEBUG -DBOOST_UBLAS_NDEBUG 
add line (before 'wmake'):

"export CPP_DEP='-DNDEBUG -DBOOST_UBLAS_NDEBUG'"

with options file should be lookin' like:

EXE_INC = \
    -I$(LIB_SRC)/finiteVolume/lnInclude \
    -I$(LIB_SRC)/meshTools/lnInclude \
    -I/usr/include/boost \
    -I$(FOAM_RUN)/myo_foamexe/include \
    $(CPP_DEP)\  

EXE_LIBS = \
    -lfiniteVolume \
    -lmeshTools


############################################
#
##(For a quick benchmark we again limit mm to 500500 with -DM_MAX=500. This time we also enable the optimized micro kernel for AVX with -DHAVE_AVX:)
#g++ -O3 -mavx -Wall -DNDEBUG -DBOOST_UBLAS_NDEBUG -DHAVE_AVX -DM_MAX=500
#g++ -Ofast -mavx -Wall -std=c++11 -DNDEBUG -DBOOST_UBLAS_NDEBUG  -DHAVE_AVX -DM_MAX=500
